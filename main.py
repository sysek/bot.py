import sys
from subprocess import call

import pyowm
import wikipedia
from botpyclass import BotPY


def menu():
    while True:
        print 'Welcome to BotPY terminal'
        print 'What you would like to do ?'
        print '0) Quit'
        print '1) Show settings'
        print '2) Connect bot'
        return int(raw_input('> '))


def settings():
    while True:
        print '---- Settings menu ----'
        print '0) Return'
        print '1) Set basics info'
        print '2) Show settings'
        int_choice = int(raw_input('> '))
        if int_choice == 1:
            call('clear')
            server = raw_input('Server: ')
            port = raw_input('Port: ')
            nick = raw_input('Nick: ')
            channel = raw_input('Channel: ')
            with open('settings', 'w+') as f:
                f.write(server)
                f.write('|' + port)
                f.write('|' + nick)
                f.write('|' + channel)
        if int_choice == 2:
            call('clear')
            try:
                with open('settings', 'r') as f:
                    bot_file = f.read().rsplit('|')
                    print 'Server: ' + bot_file[0]
                    print 'Port: ' + bot_file[1]
                    print 'Nick: ' + bot_file[2]
                    print 'Channel: ' + bot_file[3]
            except IOError:
                print 'Nie ma takiego pliku'
        if int_choice == 0:
            menu()
            break


def connect_bot():
    call('clear')
    with open('settings', 'r') as f:
        bot_settings = f.read().rsplit('|')
        bot = BotPY(bot_settings[0], int(bot_settings[1]), bot_settings[2], bot_settings[3])
        bot.connect()


while True:
    choice = menu()
    if choice == 0:
        sys.exit()
    elif choice == 1:
        settings()
    elif choice == 2:
        connect_bot()
